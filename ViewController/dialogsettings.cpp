#include "dialogsettings.h"
#include "ui_dialogsettings.h"
#include "Controller/SettingsManager.h"
#include "Constants.h"
#include <QSettings>

/*-------------------------------------------------------------------------------
 * Konstruktor & Destruktor
--------------------------------------------------------------------------------*/

DialogSettings::DialogSettings(QWidget *parent) : QDialog(parent), ui(new Ui::DialogSettings) {
    ui->setupUi(this);
    connectSlots();
    loadSettings();
}

DialogSettings::~DialogSettings() {
    delete ui;
}

/*-------------------------------------------------------------------------------
 * Methoden
--------------------------------------------------------------------------------*/

void DialogSettings::connectSlots(){
    connect(ui->rbtnApiKey,SIGNAL(toggled(bool)),this,SLOT(redmineRadioButtonsToggled()));
    connect(ui->buttonBox,SIGNAL(clicked(QAbstractButton*)),this,SLOT(dialogButtonClicked()));
}

void DialogSettings::loadSettings(){

    if(SettingsManager::instance()->redmineUseApiKeyAuthMethod){
        ui->rbtnApiKey->setChecked(true);
    }else{
        ui->rbtnBasicAuth->setChecked(true);
    }
    ui->txtUser->setText(SettingsManager::instance()->redmineUser);
    ui->txtPassword->setText(SettingsManager::instance()->redminePassword);
    ui->txtApiKey->setText(SettingsManager::instance()->redmineAPIKey);
    ui->txtServerAddress->setText(SettingsManager::instance()->redmineServerAddress);
}

void DialogSettings::writeSettings(){
    SettingsManager::instance()->redmineUseApiKeyAuthMethod = ui->rbtnApiKey->isChecked();
    SettingsManager::instance()->redmineUser = ui->txtUser->text();
    SettingsManager::instance()->redminePassword = ui->txtPassword->text();
    SettingsManager::instance()->redmineAPIKey = ui->txtApiKey->text();
    SettingsManager::instance()->redmineServerAddress = ui->txtServerAddress->text();
}

void DialogSettings::switchAuthMethod(){
    ui->txtPassword->setEnabled(!ui->txtPassword->isEnabled());
    ui->txtApiKey->setEnabled(!ui->txtApiKey->isEnabled());
    ui->txtUser->setEnabled(!ui->txtUser->isEnabled());
}

/*-------------------------------------------------------------------------------
 * UI Slots
--------------------------------------------------------------------------------*/

void DialogSettings::redmineRadioButtonsToggled(){
    switchAuthMethod();
}

void DialogSettings::dialogButtonClicked(){
    writeSettings();
    if(!SettingsManager::instance()->isValidConfigAvailable()){
        return;
    }
    accept();
    SettingsManager::instance()->writeSettings();
}


