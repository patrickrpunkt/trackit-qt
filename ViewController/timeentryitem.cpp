#include "timeentryitem.h"
#include "ui_timeentryitem.h"
#include "Controller/SettingsManager.h"
#include <QMessageBox>

/*-------------------------------------------------------------------------------
 * Konstruktor & Destruktor
--------------------------------------------------------------------------------*/

TimeEntryItem::TimeEntryItem(QWidget *parent) : QWidget(parent), ui(new Ui::TimeEntryItem) {
    ui->setupUi(this);
}

TimeEntryItem::TimeEntryItem(RedmineTimeEntry timeRecord,QWidget *parent) : QWidget(parent), ui(new Ui::TimeEntryItem),record(timeRecord) {
    ui->setupUi(this);
    QString issueDisplaytext = "#" + QString::number(record.issue->id);
    QString issueUrl = SettingsManager::instance()->redmineServerAddress + "/issues/" + QString::number(record.issue->id);
    ui->lblIssue->setText("<a href=\"" + issueUrl + "/\">"+ issueDisplaytext +"</a>");
    ui->lblIssue->setTextFormat(Qt::RichText);
    ui->lblIssue->setTextInteractionFlags(Qt::TextBrowserInteraction);
    ui->lblIssue->setOpenExternalLinks(true);

    QString projectDisplaytext = record.project->name;

    if(record.project->parentProject){
        projectDisplaytext.insert(0," ▸ ");
        projectDisplaytext.insert(0,record.project->parentProject->name);
    }

    projectDisplaytext = RedmineProject::getProjectPath(record.project,record.project->name);
    QString projectUrl = record.redmineServerUrl + "/projects/" + QString::number(record.project->id);
    ui->lblProject->setText("<a href=\"" + projectUrl + "/\">"+ projectDisplaytext +"</a>");
    ui->lblProject->setTextFormat(Qt::RichText);
    ui->lblProject->setTextInteractionFlags(Qt::TextBrowserInteraction);
    ui->lblProject->setOpenExternalLinks(true);
    ui->lblRecordDate->setDate(record.spentOn);
    ui->txtHours->setText(QString::number( record.spendTime, 'f', 2 ));
    ui->txtComment->setText(record.comments);

    fetchTimeEntryActivities();

    connect(redmineManager,SIGNAL(fetchTimeEntryActivitiesCompleted(QList<RedmineActivities*>)),this,SLOT(fetchActivitiesCompleted(QList<RedmineActivities*>)));
    connect(ui->btnPostEntry,SIGNAL(clicked()),SLOT(btnPostEntryClicked()));
    connect(ui->btnDeleteEntry,SIGNAL(clicked()),SLOT(btnDeletePostEntryClicked()));

    if (SettingsManager::instance()->redmineServerAddress != record.redmineServerUrl) {
        ui->btnPostEntry->setEnabled(false);
    }
}

TimeEntryItem::~TimeEntryItem() {
    delete ui;
    delete redmineManager;
}

/*-------------------------------------------------------------------------------
 * API Aufrufe an den Redmine Server
--------------------------------------------------------------------------------*/

void TimeEntryItem::fetchTimeEntryActivities(){
    if(SettingsManager::instance()->redmineUseApiKeyAuthMethod){
        QUrl url =  QUrl(SettingsManager::instance()->redmineServerAddress);
        redmineManager = new RedmineClient(url,SettingsManager::instance()->redmineAPIKey);
    }else{
        redmineManager = new RedmineClient(SettingsManager::instance()->redmineAccountData());
    }
    connect(redmineManager,SIGNAL(postTimeEntryCompleted(bool)),this,SLOT(postTimeEntryCompleted(bool)));

    redmineManager->getActivities();
}

void TimeEntryItem::fetchActivitiesCompleted(QList<RedmineActivities *> activities){
    int selectionIndex= -1;
    foreach (RedmineActivities *activity, activities) {
        if(record.activity->id == activity->id) {
            selectionIndex = ui->cmbActivity->count();
        }
        ui->cmbActivity->addItem(activity->name,QVariant::fromValue(activity));
    }
    if(selectionIndex != -1) {
        ui->cmbActivity->setCurrentIndex(selectionIndex);
    }

}

/*-------------------------------------------------------------------------------
 * UI Slots
--------------------------------------------------------------------------------*/

void TimeEntryItem::btnPostEntryClicked(){
    //TODO
    record.spentOn = ui->lblRecordDate->date();
    record.spendTime = ui->txtHours->text().toDouble();
    record.comments = ui->txtComment->toPlainText();
    record.activity = ui->cmbActivity->itemData(ui->cmbActivity->currentIndex()).value<RedmineActivities*>();
    redmineManager->postTimeEntry(&record);
}

void TimeEntryItem::postTimeEntryCompleted(bool withErrors){
    // händelt die benachrichtigung an den User mit einer Messagebox
    // darüber wird mitgeteilt ob die übertragung erfolgreich oder fehlerhaft war
    QMessageBox msgBox;
    if(withErrors){
        msgBox.setText("TrackIt!");
        msgBox.setInformativeText("The transmission was successful");
        msgBox.setStandardButtons(QMessageBox::Ok);
        msgBox.setDefaultButton(QMessageBox::Ok);

        QString filename = QDir::currentPath() + "/entries/" + record.uid.toString() + ".tte";
        // Datei zum Schreiben öffnen
        QFile file (filename);
        file.remove();
        file.deleteLater();
    }else{
        msgBox.setText("TrackIt! - Error");
        msgBox.setInformativeText("The transmission failed");
        msgBox.setStandardButtons(QMessageBox::Cancel | QMessageBox::Retry);
        msgBox.setDefaultButton(QMessageBox::Retry);
    }

    int ret = msgBox.exec();

    switch (ret) {
    case QMessageBox::Ok:
        emit uploadSuccessfull(this);
        break;
    case QMessageBox::Retry:
        // Don't Save was clicked
        break;
    case QMessageBox::Cancel:
        // Cancel was clicked
        break;
    default:
        // should never be reached
        break;
    }
}

void TimeEntryItem::btnDeletePostEntryClicked(){
    //TODO
    QString filename = QDir::currentPath() + "/entries/" + record.uid.toString() + ".tte";
    // Datei zum Schreiben öffnen
    QFile file (filename);
    if (file.exists()) {
        file.remove();
    }
    file.deleteLater();
    emit uploadSuccessfull(this);
}

/*-------------------------------------------------------------------------------
 * Serialisierung (Speichern und Laden der Zeiteinträge)
--------------------------------------------------------------------------------*/

void TimeEntryItem::serialize() {
    // Festlegen des Speicherverzeichnisses
    QString filename = QDir::currentPath() + "/entries/";
    // Datei zum Schreiben öffnen
    QDir dir(filename);
    // Prüfen ob das Verzeichnis existiert sonst anlegen
    if (!dir.exists()) {
        dir.mkpath(filename);
    }
    // Festlegen des Dateinamens mit pfad
    filename += record.uid.toString() + ".tte";

    QFile file (filename);

    file.open(QIODevice::WriteOnly | QIODevice::Text);

    // Datei konnte nich zum Schreiben geöffnet werden
    if (!file.isOpen()) {
        QMessageBox::warning(this, "File Save Error", "The time entry could not be saved.", QMessageBox::Ok, QMessageBox::Ok);
        return;
    }

    // Schreiben der Datei
    QTextStream ts(&file);

    ts << "[timeentry]" << endl;
    ts << "entryId:::" << record.uid.toString() << endl;
    ts << "projectId:::" << record.project->id << endl;
    ts << "projectName:::" << RedmineProject::getProjectPath(record.project,record.project->name) << endl;
    ts << "issueId:::" << record.issue->id << endl;
    ts << "comments:::" << ui->txtComment->toPlainText() << endl;
    RedmineActivities *activity = ui->cmbActivity->itemData(ui->cmbActivity->currentIndex()).value<RedmineActivities*>();
    if (activity) {
        ts << "activtyID:::" << activity->id << endl;
    }
    ts << "spendOn:::" << ui->lblRecordDate->date().toString("yyyy-MM-dd") << endl;
    ts << "hours:::" << ui->txtHours->text() << endl;
    ts << "server:::" << record.redmineServerUrl << endl;
    // Datei schließen
    file.close();
}

QList<RedmineTimeEntry> TimeEntryItem::deserialize(){
    QList<RedmineTimeEntry> items = QList<RedmineTimeEntry>();
    QStringList nameFilter("*.tte");
    QDir directory("entries/");
    // Abrufen aller Dateien im festgelegten Pfad mit der Dateiendung .tte
    QStringList timeEntryFileNames = directory.entryList(nameFilter);

    foreach(QString entryFileName, timeEntryFileNames) {
        QFile file("entries/"+entryFileName);
        if(!file.open(QIODevice::ReadOnly)) {
            QMessageBox::information(0, "error", file.errorString());
        }
        QHash<QString, QString> recordData;
        QTextStream in(&file);
        // Lesen der Datei
        while(!in.atEnd()) {
            QString line = in.readLine();
            QStringList fields = line.split(":::");
            if (fields.count() == 2) {
                recordData[fields[0]] = fields[1];
            }
        }
        // Erstellen eines TimeEntry Objektes
        RedmineTimeEntry entry = RedmineTimeEntry();
        entry.uid = QUuid(recordData["entryId"]);
        entry.comments = recordData["comments"];
        entry.project->name = recordData["projectName"];
        entry.project->id = recordData["projectId"].toInt();
        entry.issue->id = recordData["issueId"].toInt();
        entry.activity->id = recordData["activtyID"].toInt();
        QDate date = QDate().fromString(recordData["spendOn"],"yyyy-MM-dd");
        entry.spentOn = date;
        entry.spendTime = recordData["hours"].toDouble();
        entry.redmineServerUrl = recordData["server"];
        items.append(entry);
        // Schließen der Datei
        file.close();
    }
    return items;
}
