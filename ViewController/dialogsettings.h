#ifndef DIALOGSETTINGS_H
#define DIALOGSETTINGS_H

#include <QDialog>
#include <QAbstractButton>

namespace Ui {
class DialogSettings;
}

class DialogSettings : public QDialog
{
    Q_OBJECT

public:
    /* ---------------------------------------------------------------------------------------------------------------------------------
     * Konstruktor & Destruktor
     * ------------------------------------------------------------------------------------------------------------------------------- */
    explicit DialogSettings(QWidget *parent = 0);
    ~DialogSettings();
private:
    /* ---------------------------------------------------------------------------------------------------------------------------------
     * Variablen
     * ------------------------------------------------------------------------------------------------------------------------------- */
    Ui::DialogSettings *ui;              /*!< Variable zum Zugriff auf die UI Elemente die im Designer erstellt wurden: */
    /* ---------------------------------------------------------------------------------------------------------------------------------
     * Methods
     * ------------------------------------------------------------------------------------------------------------------------------- */

    //! Stellt die Verbindungen zwischen den Events und den dazugehörigen Methoden her
    void connectSlots();
    //! Läd die Einstellungen über den Settingsmanager
    void loadSettings();
    //! Speichert die Einstellungen über den Settingsmanager
    void writeSettings();
    //! Tauscht die Authentifizierungsmethoden
    void switchAuthMethod();

private slots:
    //! Wird Aufgerufen wenn der Nutzer die Authentifizierungsmethode ändert
    void redmineRadioButtonsToggled();

    //! Wird Aufgerufen wenn einer der Dialogbuttons gedrückt wird1
    void dialogButtonClicked();
};

#endif // DIALOGSETTINGS_H
