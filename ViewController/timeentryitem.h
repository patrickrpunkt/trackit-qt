#ifndef TIMEENTRYITME_H
#define TIMEENTRYITME_H
#include "Models/RedmineTimeEntry.h"
#include "Models/RedmineActivities.h"
#include "Controller/Redmine/RedmineClient.h"

#include <QWidget>

namespace Ui {
class TimeEntryItem;
}

class TimeEntryItem : public QWidget
{
    Q_OBJECT
public:
    /* ---------------------------------------------------------------------------------------------------------------------------------
     * Konstruktor & Destruktor
     * ------------------------------------------------------------------------------------------------------------------------------- */
    //! Konstruktor
    /*!
      \param parent
    */
    explicit TimeEntryItem(QWidget *parent = 0);

    //! Konstruktor zum Initialisieren mit einem Zeiteintrag
    /*!
      \param timeRecord Enthält den Zeiteintrag der über das Steuerelement angezeigt werden soll
      \param parent
    */
    explicit TimeEntryItem(RedmineTimeEntry timeRecord, QWidget *parent = 0);
    ~TimeEntryItem();

signals:
    //! Wird Aufgerufen wenn der Upload des Zeiteintrags erfolgreich war.
    /*!
      \param entry Enthält den Zeiteintrag der an den Server gesendet wurde.
    */
    void uploadSuccessfull(TimeEntryItem *entry);

public slots:
    //! Händelt den Response nach dem die Aktivitätsdaten vom Server abgerufen wurden
    /*!
      \param activities Enthält die Aktivitäten die vom Server abgerufen wurden
    */
    void fetchActivitiesCompleted(QList<RedmineActivities*> activities);

    //! Wird aufgerufen wenn der Button zum Senden eines Zeiteintrags an den Server gedrückt wurde
    void btnPostEntryClicked();

    //! Wird aufgerufen wenn der Button zum Löschen eines Zeiteintrags gedrückt wurde
    void btnDeletePostEntryClicked();

    //! Wird aufgerufen wenn ein Zeiteintrag erfolgreich an den Server übertragen wurde
    /*!
      \param withErrors Gibt an ob ein Fehler aufgetreten ist oder nicht
    */
    void postTimeEntryCompleted(bool withErrors);

    //! Speichert einen Aufwand (Eintrag) als Textdatei
    void serialize();

    //! Läd alle Aufwände (Eintrag) die auf dem lokalen Dateisystem abgelegt wurden.
    /*!
      \return Die gespeicherten Aufwände vom Typ RedmineTimeEntry
    */
    static QList<RedmineTimeEntry> deserialize();
private:
    Ui::TimeEntryItem *ui;              /*!< Variable zum Zugriff auf die UI Elemente die im Designer erstellt wurden */
    RedmineTimeEntry record;            /*!< Beinhaltet den erfassten Zeiteintrag der über das UI Element dargestellt wird */
    RedmineClient *redmineManager;      /*!< Dient zur Steuerung der Abfragen an den Server */

    //! Senden eine Anfrage an den Server um alle verfügbaren Aktivitäten abzurufen.
    void fetchTimeEntryActivities();
};

#endif // TIMEENTRYITME_H
