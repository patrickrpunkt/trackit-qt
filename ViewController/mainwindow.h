#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QtCore>
#include <QMainWindow>
#include <QLabel>
#include <QProgressBar>
#include "Controller/Redmine/RedmineClient.h"
#include "Controller/TimeTracker.h"
#include "ViewController/timeentryitem.h"
#include "Models/RedmineTimeEntry.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    /* ---------------------------------------------------------------------------------------------------------------------------------
     * Methods
     * ------------------------------------------------------------------------------------------------------------------------------- */
    //! Erstellt und passt vorhandene UI Elemente an
    void setupCustomUI();
    //! Stellt die Verbindungen zwischen den Events und den dazugehörigen Methoden her
    void connectSlots();
    //! Ruf die Daten vom Redmine Server ab
    void reloadRedmineData();
    //! Bereitet den gerade aufgenommenen Zeiteintrag für die Anzeige an der Oberfläche vor
    void prepareTimeEntryForDisplay();
    //! Bereitet den übergebenen Zeiteintrag für die Anzeige an der Oberfläche vor
    /*!
      \param entryItem Der Eintrag der an der Oberfläche angezeigt werden soll
    */
    void prepareTimeEntryForDisplay(RedmineTimeEntry entryItem);
    //! Läd alle noch nicht auf dem Server gespeicherten Einträge vom lokalen Dateisystem um sie an der Oberfläche anzuzeigen
    void loadUnpublishedEntries();
    //! Speichert alle noch nicht auf dem Server hochgeladenen Einträge auf dem lokalen Dateisystem
    void persistTimeEntries();

    /* ---------------------------------------------------------------------------------------------------------------------------------
     * UI
     * ------------------------------------------------------------------------------------------------------------------------------- */
    Ui::MainWindow *ui;                         /*!< Variable zum Zugriff auf die UI Elemente die im Designer erstellt wurden */
    QLabel *lblTimer;                           /*!< Label zur Anzeige der Zeit für die aktuelle Aufnahme */
    QProgressBar *progress;                     /*!< Progressbar für die Anzeige der Netzwerkaktivität beim aktualisieren der Redminedaten */

    /* ---------------------------------------------------------------------------------------------------------------------------------
     * Variablen
     * ------------------------------------------------------------------------------------------------------------------------------- */
    TimeTracker *trackerController;             /*!< Objekt zum Steuern des der Zeitaufnahme  */
    RedmineClient *redmineManager;              /*!< Dient zur Steuerung der Abfragen an den Server */
    RedmineTimeEntry currentTimeRecord;         /*!< Hält den aktuell aufgenommenen Eintrag */

protected:
    //! Überschreibt das Close Event um in diesem noch nicht hochgeladene Zeiteinträge lokal zuspeichern
    /*!
      \param event über dieses Objekt kann das beenden des Programms abgebrochen werden
    */
    void closeEvent(QCloseEvent *event);

private slots:
    //! Über diesen Button wird die Erfassung eines neuen Zeiteintrags gestartet
    /*!
      \param toggled Gibt an ob die aufnahme gestartet oder beendet wird/ist
    */
    void btnRecordTimeClicked(bool toggled);

    //! Der Einstellungsdialog wird geöffnet
    void btnSettingsClicked();

    //! Ein Neuladen der Daten vom Server wird angestoßen
    void btnRefreshClicked();

    //! Händelt den Response nach dem die Projektdaten vom Server abgerufen wurden
    /*!
      \param projects Enthält die Projekte die vom Server abgerufen wurden
    */
    void fetchProjectCompleted(QList<RedmineProject*> projects);

    //! Händelt den Response nach dem die Ticketdaten vom Server abgerufen wurden
    /*!
      \param issues Enthält die Tickets die vom Server abgerufen wurden
    */
    void fetchIssuesCompleted(QMap<int,RedmineIssue*> issues);

    //! Händelt das Verhalten wenn ein Projekt ausgewählt wurde
    /*!
      \param idx Enthält den Index des Elements in der Liste das jetzt ausgewählt wurde
    */
    void cmbProjectsSelectedIndexChanged(int idx);

    //! Händelt das Verhalten wenn ein Ticket ausgewählt wurde
    /*!
      \param idx Enthält den Index des Elements in der Liste das jetzt ausgewählt wurde
    */
    void cmbIssuesSelectedIndexChanged(int idx);

    //! Aktualisiert die UI immer dann wenn sich der Timer aktualisiert
    /*!
      \param timeString Enthält die momentan abgelaufene Zeit Formatiert für die Anzeige an der Oberfläche
    */
    void updateTimer(QString timeString);

    //! Händelt das Verhalten wenn ein Zeiteintrag erfolgreich an den Server übermittelt wurde
    /*!
      \param entry Enthält den Eintrag der übermittelt wurde
    */
    void timeEntryUploadSuccessfull(TimeEntryItem *entry);

    //! Händelt das Verhalten wenn ein Fehler bei der Komunikation mit dem Server aufgetreten ist.
     /*!
      \param message Enthält die Fehlernachricht zur Anzeige für den Benutzer
    */
    void handleError(QString message);
};

#endif // MAINWINDOW_H
