m#-------------------------------------------------
#
# Project created by QtCreator 2014-09-16T11:30:42
#
#-------------------------------------------------

QT       += core gui
QT       += network xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TrackIt
TEMPLATE = app

CONFIG-= de.appileptiCoding.trackIt

SOURCES += main.cpp\
        ViewController/mainwindow.cpp \
    ViewController/dialogsettings.cpp \
    Controller/Redmine/KeyAuthenticator.cpp \
    Controller/Redmine/PasswordAuthenticator.cpp \
    Controller/Redmine/RedmineClient.cpp \
    Controller/TimeTracker.cpp \
    Controller/SettingsManager.cpp \
    Models/RedmineAccount.cpp \
    ViewController/timeentryitem.cpp \
    Models/RedmineActivities.cpp \
    Models/RedmineTimeEntry.cpp \
    Models/RedmineProject.cpp \
    Models/RedmineIssue.cpp

HEADERS  += ViewController/mainwindow.h \
    ViewController/dialogsettings.h \
    Controller/Redmine/IAuthenticator.h \
    Controller/Redmine/KeyAuthenticator.h \
    Controller/Redmine/PasswordAuthenticator.h \
    Controller/Redmine/RedmineClient.h \
    Controller/TimeTracker.h \
    Controller/SettingsManager.h \
    Constants.h \
    Models/RedmineAccount.h \
    ViewController/timeentryitem.h \
    Models/RedmineActivities.h \
    Models/RedmineTimeEntry.h \
    Models/RedmineProject.h \
    Models/RedmineIssue.h

FORMS    += Views/mainwindow.ui \
    Views/dialogsettings.ui \
    Views/timeentryitem.ui

RESOURCES += \
    resources.qrc

RC_ICONS = Time.ico
ICON = Time.icns
