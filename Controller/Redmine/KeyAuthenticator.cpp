#include "KeyAuthenticator.h"
#include "Constants.h"
#include <QtNetwork/QNetworkRequest>

/* -------------------------------------------
 *  Konstrukor
 * ----------------------------------------- */
KeyAuthenticator::KeyAuthenticator(QByteArray apiKey) : _apiKey(apiKey) {
}

/* -------------------------------------------
 *  Setzt den Header im übergebenen Request
 *  mit dem API KEY
 * ----------------------------------------- */
void KeyAuthenticator::addAuthentication(QNetworkRequest* request) {
    request->setRawHeader(API_HTTP_HEADER_REDMINE_APIKEY, _apiKey);
}
