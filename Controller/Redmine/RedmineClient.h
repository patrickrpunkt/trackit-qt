#ifndef REDMINE_CLIENT_H
#define REDMINE_CLIENT_H
class IAuthenticator;
class QNetworkAccessManager;
#include <QtCore/QObject>
#include <QtCore/QString>
#include <QtCore/QUrl>
#include <QtNetwork/QNetworkAccessManager>
#include <Models/RedmineAccount.h>
#include <Models/RedmineProject.h>
#include <Models/RedmineIssue.h>
#include <Models/RedmineActivities.h>
#include <Models/RedmineTimeEntry.h>

/* Redmine API Klasse
 *
 * Händelt alle Verbindungsaufrufe zur Redmine Instance
 */
class RedmineClient : public QObject {
	Q_OBJECT

signals:
    //! Wird aufgerufen wenn alle Projekte vom Server abgerufen wurden
    /*!
      \param projects Enthält alle abgerufenen Projekte
    */
    void fetchRedmineProjectsCompleted(QList<RedmineProject*> projects);

    //! Wird aufgerufen wenn alle Tickets zu einem Projekt vom Server abgerufen wurden
    /*!
      \param issues Enthält alle abgerufenen Tickets
    */
    void fetchProjectIssuesCompleted(QMap<int,RedmineIssue*> issues);

    //! Wird aufgerufen wenn alle Aktivitäten vom Server abgerufen wurden
    /*!
      \param activities Enthält alle abgerufenen Aktivitäten
    */
    void fetchTimeEntryActivitiesCompleted(QList<RedmineActivities*> activities);

    //! Wird aufgerufen wenn die Übertragung des Eintrags an den Server abgeschlossen wurde
    /*!
      \param withErrors Gibt an ob die Übertragung erfolgreich war oder nicht
    */
    void postTimeEntryCompleted(bool withErrors);

    //! Wird aufgerufen wenn ein Fehler bei der Komunikation mit dem Server auftritt
    /*!
      \param message Enthält eine Fehlermeldung für die Nutzerausgabe
    */
    void apiErrorOccurs(QString message);

private slots:
    void finishedProjectsRequest();
    void finishedIssuesRequest();
    void finishedActivitiesRequest();
    void finishedTimeEntryRequest();
    void handleError();

public:
	enum EMode {
		GET,
		POST,
		PUT,
		DELETE
	};

    //! Initialisieren des Clients mit einem RedmineAccount
    RedmineClient(RedmineAccount *account);

    //! Initialisierung eines Client Objekts mit einem API Key für die Authentifizierung
    RedmineClient(QUrl url, QString apiKey, QObject* parent = NULL);

    //! Initialisierung eines Client Objekts mit einem Benutzername und Password für die Authentifizierung
    RedmineClient(QUrl url, QString login, QString password);
	virtual ~RedmineClient();

    //! Ruft alle Projekte vom Server ab die für den angebenen User verfügbar sind.
    /*!
    */
    void getProjects();

    //! Ruft alle Tickets vom Server ab die zum angefragten Projekt gehören.
    /*!
      \param projectId Identifiziert das Projekt für das die Tickets abgerufen werden sollen
    */
    void getIssues(QString projectId);

    //! Ruft alle Aktivitäten vom Server ab die für den angebenen User verfügbar sind.
    /*!
    */
    void getActivities();

    //! Veröffentlicht einen Zeiteintrag auf dem Server
    /*!
      \param entry Der Eintrag der auf dem Server veröffentlicht werden soll
    */
    void postTimeEntry(RedmineTimeEntry *entry);

private:
    QUrl _url;                                      /*!< URL and die der Request gesendet wird */
    IAuthenticator*	_authenticator;                 /*!< Objekt das die Art der Authentifizierung festlegt */
    QNetworkAccessManager* _networkManager;         /*!< QT Objekt das die Netzwerkkomunikation händelt */
    QByteArray _userAgent;                          /*!< Enthält den User Agent */
    QNetworkReply* _currentReply;                   /*!< Enthält die Antwort der aktuellen Serveranfrage */

    /* -------------------------------------------
     *  Bereitet die Anfrage an den Server vor
     * ----------------------------------------- */
    QNetworkRequest prepareRequest(QUrl url);

    void init();
};

#endif
