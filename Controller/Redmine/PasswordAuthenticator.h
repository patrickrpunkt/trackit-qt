#ifndef PASSWORD_AUTHENTICATOR_H
#define PASSWORD_AUTHENTICATOR_H

#include "IAuthenticator.h"
#include <QtCore/QString>


/* ---------------------------------------------------------------------------------------------------------------------------------
 *  Basic login and password authenticator
 *  Händelt die Authentifizierung über Basic Auth
 * ------------------------------------------------------------------------------------------------------------------------------- */
class PasswordAuthenticator : public IAuthenticator {
public:
	PasswordAuthenticator(QString login, QString password);
	virtual ~PasswordAuthenticator() {}
	virtual void addAuthentication(QNetworkRequest* request);

private:
	QString	_login;
	QString	_password;
};

#endif // PASSWORD_AUTHENTICATOR_H
