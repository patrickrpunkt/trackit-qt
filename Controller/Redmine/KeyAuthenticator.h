#ifndef KEY_AUTHENTICATOR_H
#define KEY_AUTHENTICATOR_H

#include "IAuthenticator.h"

#include <QtCore/QByteArray>


/* ---------------------------------------------------------------------------------------------------------------------------------
 *  API key authenticator
 *  Händelt die Authentifizierung über einen API Key
 * ------------------------------------------------------------------------------------------------------------------------------- */
class KeyAuthenticator : public IAuthenticator {
public:
	KeyAuthenticator(QByteArray apiKey);
	virtual ~KeyAuthenticator() {}
	virtual void addAuthentication(QNetworkRequest* request);

private:
	QByteArray	_apiKey;
};

#endif // KEY_AUTHENTICATOR_H
