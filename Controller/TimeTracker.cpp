#include "TimeTracker.h"

/* -------------------------------------------
 *
 *
 * ----------------------------------------- */
TimeTracker::TimeTracker(){
    timer = new QTimer(this);
    spendTime = 0;
    connect(timer,SIGNAL(timeout()),this,SLOT(tick()));
}

/* -------------------------------------------
 *
 *
 * ----------------------------------------- */
TimeTracker::~TimeTracker(){
}

/* -------------------------------------------
 *
 *
 * ----------------------------------------- */
void TimeTracker::resetTime(){
    time = new QTime(0,0,0,0);
    temp_seconds=0;
    stopTime();
}

/* -------------------------------------------
 *
 *
 * ----------------------------------------- */
void TimeTracker::startTime(){
    time = new QTime();
    time->setHMS(0,0,0,0);
    timer->start(1000);
}

/* -------------------------------------------
 *
 *
 * ----------------------------------------- */
void TimeTracker::stopTime() {
    timer->stop();
}

/* -------------------------------------------
 *
 *
 * ----------------------------------------- */
QTime* TimeTracker::currentTime(){
    return time;
}


/* -------------------------------------------
 *
 *
 * ----------------------------------------- */
void TimeTracker::tick(){
    QTime newtime;
    temp_seconds += 1;
    newtime.setHMS(0,0,0,0);
    newtime = time->addSecs(temp_seconds);
     QString timeString;
    if(newtime.hour() > 0){
        timeString = newtime.toString("hh:mm:ss");
    }else if(newtime.hour() == 0 && newtime.minute() > 0){
        timeString = newtime.toString("m:ss")+ " min";
    }else if(newtime.hour() == 0 && newtime.minute() == 0){
        timeString = newtime.toString("s") + " sec";
    }else{
        timeString = newtime.toString("hh:mm:ss");
    }

    double hours = newtime.hour();
    double minutes = newtime.minute();
    spendTime = hours;
    spendTime += minutes/60;

    emit update(timeString);
}

/* -------------------------------------------
 *
 *
 * ----------------------------------------- */
double TimeTracker::getHours(){
    return spendTime;
}
