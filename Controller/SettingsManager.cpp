#include "SettingsManager.h"
#include "Constants.h"
#include <QSettings>

SettingsManager* SettingsManager::_instance = 0;

/* -------------------------------------------
 *  Läd alle Einstellungen vom angegebene Pfad
 * ----------------------------------------- */
void SettingsManager::loadSettings() {
    QString filepath = QCoreApplication::applicationDirPath() + QString("/config.ini");
    QSettings settings(filepath, QSettings::IniFormat);
    // Auslesen der Settings
    this->redmineUseApiKeyAuthMethod = settings.value(CONFIG_REDMINE_API_MODE_ENABLED).toBool();;
    this->redmineUser = settings.value(CONFIG_REDMINE_USERNAME).toString();
    this->redminePassword = settings.value(CONFIG_REDMINE_PASSWORD).toString();
    this->redmineAPIKey = settings.value(CONFIG_REDMINE_APIKEY).toString();
    this->redmineServerAddress = settings.value(CONFIG_REDMINE_SERVERADDRESS).toString();
}

/* -------------------------------------------
 *  Schreibt alle Einstellungen an den
 *  angegebene Pfad in eine INI Datei
 * ----------------------------------------- */
void SettingsManager::writeSettings() {
    // definition des Pfades
    QString filepath = QCoreApplication::applicationDirPath() + QString("/config.ini");
    QSettings settings(filepath, QSettings::IniFormat);
    settings.setValue(CONFIG_REDMINE_SERVERADDRESS,redmineServerAddress);
    settings.setValue(CONFIG_REDMINE_USERNAME,redmineUser);
    settings.setValue(CONFIG_REDMINE_PASSWORD,redminePassword);
    settings.setValue(CONFIG_REDMINE_APIKEY,redmineAPIKey);
    settings.setValue(CONFIG_REDMINE_API_MODE_ENABLED,redmineUseApiKeyAuthMethod);
}

/* -------------------------------------------
 *  Überprüft ob die geladenen Einstellungen
 *  gültig sind
 * ----------------------------------------- */
bool SettingsManager::isValidConfigAvailable() {
    // Pürfung auf gülitige Server Adresse
    if(redmineServerAddress.isNull() || redmineServerAddress.isEmpty()){
        return false;
    }
    //Prüfung der einzelenen Authentifizierungsmethoden auf gültigkeit
    if(redmineUseApiKeyAuthMethod && !redmineAPIKey.isEmpty()){
        return true;
    }else if(!redmineUseApiKeyAuthMethod && !redmineUser.isEmpty() && !redminePassword.isEmpty()){
        return true;
    }
    return false;
}

/* -------------------------------------------
 *  Gibt die geladen Einstellungen in Form
 *  eines RedmineAccount zurück
 * ----------------------------------------- */
RedmineAccount* SettingsManager::redmineAccountData() {
    RedmineAccount *account = new RedmineAccount(redmineServerAddress,redmineUser,redminePassword);
    return account;
}

