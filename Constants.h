#ifndef CONSTANTS
#define CONSTANTS

/* -------------------------------------------
 *
 *
 * ----------------------------------------- */
#define CONFIG_REDMINE_USERNAME  "redmine_user"
#define CONFIG_REDMINE_PASSWORD  "redmine_password"
#define CONFIG_REDMINE_APIKEY  "api_key"
#define CONFIG_REDMINE_SERVERADDRESS  "redmine_server_address"
#define CONFIG_REDMINE_API_MODE_ENABLED  "redmine_radio_api"

#define API_HTTP_HEADER_REDMINE_APIKEY "X-Redmine-API-Key"

#define MAINWINDOW_MESSAGE_FINISH_TIME_RECORDING  "The time recording has been stopped."
#define MAINWINDOW_QUESTION_FINISH_TIME_RECORDING "Do you want to save your timeentry?"

#endif // CONSTANTS

