#ifndef REDMINEACCOUNT_H
#define REDMINEACCOUNT_H

#include <QtCore/QString>
#include <QtCore/QUrl>

class RedmineAccount
{
public:
    /* ---------------------------------------------------------------------------------------------------------------------------------
     * Variablen
     * ------------------------------------------------------------------------------------------------------------------------------- */
    QUrl hostUrl;                   /*!< Die Url zum Redmine Server */
    QString username;               /*!< Der Benutzername der zur Authentifizierung genutzt werden soll */
    QString password;               /*!< Das Passwort der zur Authentifizierung genutzt werden soll */
    int portNumber;                 /*!< Gibt den Port an unter dem die Redmine Instanz zufinden ist */
    bool useSSL;                    /*!< Gibt an ob SSL genutzt werden soll */

    /* ---------------------------------------------------------------------------------------------------------------------------------
     * Public Methoden
     * ------------------------------------------------------------------------------------------------------------------------------- */
    //! Gibt einen Projekt Pfad vom Root Projekt zum aktuellen zurück
    /*!
      \param hostUrl Die Url zum Redmine Server
      \param username Der Benutzername der zur Authentifizierung genutzt werden soll
      \param password Das Passwort der zur Authentifizierung genutzt werden soll
      \return Eine Instanz vom Typ RedmineAccount mit den übergegebenen Werten
    */
    RedmineAccount(QUrl hostUrl,QString username,QString password);
};

#endif // REDMINEACCOUNT_H
