#include "RedmineIssue.h"
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

/* -------------------------------------------
 *  Model zu Darstellung eines Tickets
 *  innerhalb eines Projekts
 * ----------------------------------------- */

/* -------------------------------------------
 *  Konstruktor
 * ----------------------------------------- */
RedmineIssue::RedmineIssue(QObject *parent) : QObject(parent) {
    subject = "";
    assignedTo = "";
    description = "";
    tracker = "";
}

/* -------------------------------------------
 *  Destruktor
 * ----------------------------------------- */
RedmineIssue::~RedmineIssue() {
}

/* -------------------------------------------
 *  Parst einen JSON String zu einer Liste
 *  von Issues
 * ----------------------------------------- */
QMap<int,RedmineIssue*> RedmineIssue::createIssuesFromJson(QString jsonString) {
    //TODO
    QMap<int,RedmineIssue*> issues = QMap<int,RedmineIssue*>();
    QJsonDocument jsonResponse = QJsonDocument::fromJson(jsonString.toUtf8());
    QJsonObject jsonObject = jsonResponse.object();
    QJsonArray jsonArray = jsonObject["issues"].toArray();

    foreach (const QJsonValue & value, jsonArray)
    {
        QJsonObject obj = value.toObject();
        RedmineIssue *issue = new RedmineIssue();
        issue->id =obj["id"].toInt();
        issue->subject = obj["subject"].toString();
        issue->assignedTo = getContentFromJsonArray(obj,"assigned_to","name");
        issue->tracker = getContentFromJsonArray(obj,"tracker","name");
        issue->status = getContentFromJsonArray(obj,"status","name");
        issue->description =obj["description"].toString();
        issues.insert(issue->id,issue);
    }
    return issues;
}

/* -------------------------------------------
 *  Gibt den Inhalt eines JSON Arrays zurück
 * ----------------------------------------- */
QString RedmineIssue::getContentFromJsonArray(QJsonObject obj,QString arrayName, QString fieldname) {
    QJsonValue valueObj = obj[arrayName];
    return valueObj.toObject()[fieldname].toString();
}
