#ifndef REDMINEPROJECT_H
#define REDMINEPROJECT_H
#include <QtCore/qdatetime.h>
#include <QMap>
#include <QMetaType>
class RedmineProject
{
public:
    RedmineProject();

    /* ---------------------------------------------------------------------------------------------------------------------------------
     * Variablen
     * ------------------------------------------------------------------------------------------------------------------------------- */
    int id;                             /*!< Die Datenbank ID */
    QString name;                       /*!< Der Name des Projektes */
    QString identifier;                 /*!< Eindeutiger Bezeichner des Projektes */
    QString description;                /*!< Beschreibung zum Projekt */
    int status;                         /*!< Aktueller Status des Projektes */
    QDate createdOn;                    /*!< Wann wurde das Projekt auf dem Server angelegt */
    RedmineProject *parentProject;      /*!< Das Hauptprojekt des aktuellen Projektes */

    /* ---------------------------------------------------------------------------------------------------------------------------------
     * Methoden
     * ------------------------------------------------------------------------------------------------------------------------------- */

    //! Parst einen JSON String zu einer Liste von Projekten.
    /*!
      \param jsonString Enthält die zu parsenden Projekte als JSON String
      \return Die Liste der geparsten Projekte
    */
    static QList<RedmineProject*> createProjectsFromJson(QString jsonString);

    //! Gibt einen Projekt Pfad vom Root Projekt zum aktuellen zurück
    /*!
      \param project Das Projekt von dem aus der Pfad aufgebaut werden soll
      \param projectname default -> Leer String
      \return Der Projekt Pfad als String
    */
    static QString getProjectPath(RedmineProject *project,QString projectname);
};
Q_DECLARE_METATYPE(RedmineProject*)
#endif // REDMINEPROJECT_H


