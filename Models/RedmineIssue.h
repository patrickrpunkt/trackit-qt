#ifndef REDMINEISSUE_H
#define REDMINEISSUE_H

#include <QObject>
#include <QMap>
#include <QMetaType>

class RedmineIssue : public QObject
{
    Q_OBJECT
public:

    /* ---------------------------------------------------------------------------------------------------------------------------------
     * Variablen
     * ------------------------------------------------------------------------------------------------------------------------------- */
    int id; /*!< Die Datenbank ID */
    QString subject;                        /*!< Der Titel des aktuellen Tickets */
    QString assignedTo;                     /*!< Gibt an welcher Person das Ticket zugewiesen ist. */
    QString description;                    /*!< Die Beschreibung des Tickets */
    QString tracker;                        /*!< Der Tracker beschreibt die Art des Tickets (Aufgabe, Bug, etc.) */
    QString status;                         /*!< Aktueller Status des Tickets */

    /* ---------------------------------------------------------------------------------------------------------------------------------
     * Public Methoden
     * ------------------------------------------------------------------------------------------------------------------------------- */
    explicit RedmineIssue(QObject *parent = 0);
    ~RedmineIssue();

    //! Parst einen JSON String zu einer Liste von Tickets.
    /*!
      \param jsonString Enthält die zu parsenden Tickets als JSON String
      \return Die Liste der geparsten Tickets
    */
    static QMap<int,RedmineIssue*> createIssuesFromJson(QString jsonString);

private:
    /* ---------------------------------------------------------------------------------------------------------------------------------
     * Private Methoden
     * ------------------------------------------------------------------------------------------------------------------------------- */
    //! Gibt den JSON Value aus einen Array innerhalb des JSONs
    /*!
      \param obj Enthält das JSON in dem sich die Werte befinden
      \param arrayName Arrayname innerhalb des Root Objektes
      \param fieldname Feldname innerhalb des Arrays
      \return Gibt den Wert des Angegebenen Feldes innerhalb des Array zurück
    */
     static QString getContentFromJsonArray(QJsonObject obj,QString arrayName, QString fieldname);
};
Q_DECLARE_METATYPE(RedmineIssue*)

#endif // REDMINEISSUE_H
