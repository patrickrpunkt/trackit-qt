#include "RedmineProject.h"
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

/* -------------------------------------------
 *  Model zu Darstellung eines Projektes
 *  auf einem Redmine Server
 * ----------------------------------------- */

/* -------------------------------------------
 *  Konstruktor
 * ----------------------------------------- */
RedmineProject::RedmineProject() {
    id = -1;
    identifier = "xxx-xxx-xxx";
    name = "unknown";
    parentProject = NULL;
}

/* -------------------------------------------
 *  Parst einen JSON String zu einer Liste
 *  von Projekten
 * ----------------------------------------- */
QList<RedmineProject*> RedmineProject::createProjectsFromJson(QString jsonString) {
    //TODO
    QList<RedmineProject*> projects = QList<RedmineProject*>();
    QMap<int,RedmineProject*> projectsMap = QMap<int,RedmineProject*>();
    QJsonDocument jsonResponse = QJsonDocument::fromJson(jsonString.toUtf8());
    QJsonObject jsonObject = jsonResponse.object();
    QJsonArray jsonArray = jsonObject["projects"].toArray();

    try {
        foreach (const QJsonValue & value, jsonArray) {
            QJsonObject obj = value.toObject();
            RedmineProject *project = new RedmineProject();
            project->id =obj["id"].toInt();
            project->name =obj["name"].toString();
            project->identifier =obj["identifier"].toString();
            project->description =obj["description"].toString();
            if(obj.contains("parent"))
            {
                QJsonObject subObj = obj["parent"].toObject();
                RedmineProject *parentProject = projectsMap.find(subObj["id"].toInt()).value();
                project->parentProject = parentProject;
            }
            projectsMap.insert(project->id,project);
            projects.append(project);
        }
    }
    catch(std::exception & e){
    }

    return projects;
}

/* -------------------------------------------
 *  Konstruktor
 * ----------------------------------------- */
QString RedmineProject::getProjectPath(RedmineProject *project,QString projectname){
    if(project->parentProject){
        projectname.insert(0," » ");
        projectname.insert(0,project->parentProject->name);
        return getProjectPath(project->parentProject,projectname);
    }
    return projectname;
}
