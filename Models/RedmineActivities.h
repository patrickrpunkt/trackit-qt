#ifndef REDMINEACTIVITIES_H
#define REDMINEACTIVITIES_H

#include <QObject>
#include <QMetaType>

class RedmineActivities
{
public:
    RedmineActivities();
    ~RedmineActivities();

    /* ---------------------------------------------------------------------------------------------------------------------------------
     * Variablen
     * ------------------------------------------------------------------------------------------------------------------------------- */
    int id;                 /*!< Die Datenbank ID */
    QString name;           /*!< Der Anzeige Name der Aktivität */

    /* ---------------------------------------------------------------------------------------------------------------------------------
     * Public Methoden
     * ------------------------------------------------------------------------------------------------------------------------------- */
    //! Parst einen JSON String zu einer Liste von Aktivitäten.
    /*!
      \param jsonString Enthält die zu parsenden Aktivitäten als JSON String
      \return Die Liste der geparsten Aktivitäten
    */
    static QList<RedmineActivities*> createActivitiesFromJson(QString jsonString);



};
Q_DECLARE_METATYPE(RedmineActivities*)
#endif // REDMINEACTIVITIES_H
