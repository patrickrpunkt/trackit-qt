#include "RedmineAccount.h"
#include <QtCore/QUrl>

/* -------------------------------------------
 *  Model zu Darstellung eines Accounts
 *  auf einem Redmine Server
 * ----------------------------------------- */

/* -------------------------------------------
 *  Konstruktor
 * ----------------------------------------- */
RedmineAccount::RedmineAccount(QUrl hostUrl, QString username, QString password) {
    this->hostUrl = hostUrl;
    this->username = username;
    this->password = password;
    this->useSSL = true;
    this->portNumber = 80;
}
